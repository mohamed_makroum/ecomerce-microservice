package com.medmk.apigatewayconfiguration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class ApiGatewayConfigurationApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiGatewayConfigurationApplication.class, args);
	}

	@Bean
	public RouteLocator gatewayRoutes(RouteLocatorBuilder routeLocatorBuilder)
	{
		return routeLocatorBuilder.routes()
				.route("customer", rt -> rt.path("/api/product/**")
						.uri("lb://product-service"))
				.build();

	}
}
